


const { app, BrowserWindow, globalShortcut, Menu } = require('electron')

function createWindow () {
	// Create the browser window.
	win = new BrowserWindow({
		width: 800,
		height: 600,
		transparent: true,
		frame: false,
		// kiosk: true,
		movable: false,
		alwaysOnTop: true,
		fullscreenable: false,
		hasShadow: false
	});
	win.maximize();

	// and load the index.html of the app.
	win.loadFile('index.html')

	globalShortcut.register('CommandOrControl+Shift+1', () => {
		console.log('CommandOrControl+Shift+1 is pressed');
		win.focus();
	})
}

app.on('ready', createWindow);



const template = [
  {
    role: 'window',
    submenu: [
      { role: 'minimize' },
      { role: 'close' }
    ]
  }
];
if (process.platform === 'darwin') {
	template.unshift({
		label: app.getName(),
		submenu: [
			{ role: 'about' },
			{ role: 'quit' }
		]
	});
}


const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)
