





var pos = {x:0,y:0};

var selecting = false;
document.addEventListener('mousedown', (ev)=>{
	selecting = true;

	pos.x = ev.clientX;
	pos.y = ev.clientY;

	document.getElementById('top').style.height = ev.clientY;
	document.getElementById('bottom').style.height = window.innerHeight - ev.clientY;

	document.getElementById('left').style.top = ev.clientY;
	document.getElementById('left').style.width = ev.clientX;
	document.getElementById('left').style.height = 0;

	document.getElementById('right').style.top = ev.clientY;
	document.getElementById('right').style.width = ev.clientX;
	document.getElementById('right').style.height = 0;

});

document.addEventListener('mousemove', (ev)=>{
	if (selecting) {

		/* Side widths */
		if (ev.clientX > pos.x){ // dragging right
			var rightWidth = window.innerWidth - ev.clientX;
			document.getElementById('right').style.width = rightWidth;
			document.getElementById('left').style.width = pos.x;
		}else{ // dragging left
			document.getElementById('left').style.width = ev.clientX;
			var rightWidth = window.innerWidth - pos.x;
			document.getElementById('right').style.width = rightWidth;
		}

		/* Top/Bottom heights */
		if (ev.clientY > pos.y){ // dragging down
			// Top/Bottom Heights
			document.getElementById('top').style.height = pos.y;
			var bottomHeight = window.innerHeight - ev.clientY;
			document.getElementById('bottom').style.height = bottomHeight;

			// Side Heights
			var sideHeight = ev.clientY - pos.y;
			document.getElementById('left').style.height = sideHeight;
			document.getElementById('right').style.height = sideHeight;

			document.getElementById('left').style.top = pos.y;
			document.getElementById('right').style.top = pos.y;


		}else{ // dragging up
			// Top/Bottom Heights
			document.getElementById('top').style.height = ev.clientY;
			var bottomHeight = window.innerHeight - pos.y;
			document.getElementById('bottom').style.height = bottomHeight;

			// Side Heights
			var sideHeight = pos.y - ev.clientY;
			document.getElementById('left').style.height = sideHeight;
			document.getElementById('right').style.height = sideHeight;

			document.getElementById('left').style.top = ev.clientY;
			document.getElementById('right').style.top = ev.clientY;
		}
	}
});

document.addEventListener('mouseup', (ev)=>{
	selecting = false;

});
